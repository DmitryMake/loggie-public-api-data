export interface IContainer {
  readonly id: number;
  readonly type: string;
  readonly alt_names?: {
    readonly name: string;
    readonly language_code: string;
  }[];
}
