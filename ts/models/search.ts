import { IPayment } from './payment';
import { ICompany } from './company';
import { IDropOff, IRateList } from './rates';
import { ITerminal } from './terminal';

export interface IRoute {
  // Интерфейс отрезка пути
  start_point: ITerminal; // Откуда
  finish_point: ITerminal; // Куда
  after_finish_point?: ITerminal; // Точка сдачи порожнего контейнера. Используется только для автовывоза
  is_public: boolean;
  duration: number;
  tag: string;

  owner: ICompany;
  rate_list: IRateList;
  payments: IPayment;
}

export interface ISearchResult {
  path: IRoute[];
  dropoff: IDropOff;
  total_d: number; // Итого долларов США
};

export type SearchType =
  | 'o'   // own
  | 'p'   // public
  | 's';  // subscribed

