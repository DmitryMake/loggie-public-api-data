import { IContainer } from './container-type';
import { ITerminal } from './terminal';

export enum ETransportType {
    SEA = 0,
    TRAIN = 1,
    TRUCK = 2,
}

export interface IRateList {
  id: number;
  rate_kit: number;
  owner: number;
  container_rent: boolean;
  main_list: number;
  transport_type: ETransportType;
  act_time_start: Date | null;
  act_time_finish: Date | null;
}

export interface IDropOff {
  id: number;
  drop_off_list: number;
  price: number;
  price_currency: string;
  terminal: ITerminal;
  cont_type: IContainer;
}
