export interface ITerminal {
  id: number;
  name?: string;
  name_l?: string;
  city_name_l?: string;
  country_code?: string;
  population?: number;
  ru: {
    name: string;
    name_hl?: string;
    city: {
      name: string;
      name_hl?: string;
      country: {
        name: string;
      }
    }
  };
  en: {
    name: string;
    name_hl?: string;
    city: {
      name: string;
      name_hl?: string;
      country: {
        name: string;
      }
    }

  };
  city: {
    id: number;
    name: string;
    population: number;
    location: {
      lat: number;
      lon: number;
    };
    country: {
      id: number;
      name: string;
      code: string;
    }
  };
  status: TerminalType | number;
}

export type TerminalType =
  | 'Port'        // 0
  | 'Station'     // 1
  | 'Intracity'   // 2
  | 'DropStation' // 3
  | '';
