export interface ICompany {
  id: number;
  name: string;
  phone_number?: string;
  logotype?: string;
  description?: string;  
  public_logotype?: string;
  is_carrier: boolean; // является ли компания перевозчиком
  email: string;
}
