export interface IMoney {
  value: number;
  currency: string;
}

export interface ICurrency {
  sign: string;
  code: string;
  alt_names?: string[];
}

export interface IPayment {
  id: number;
  rate?: number;
  amount: number;
  amount_currency: string;
  included_tax_percent: number;
}
