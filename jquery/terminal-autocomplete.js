$(function() {
  function log(message) {
    $("<div>").text(message).prependTo("#log");
    $("#log").scrollTop(0);
  }

  $("#terminal").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "https://loggie.ru/api/v0/terminal_ac/",
        dataType: "json",
        data: {
          q: request.term,
          status: "port", // "intracity", "station" 
          exclude: 42
        },
        success: function(data) {
          var bucket = [];
          $.each(data, function(index, value) {
            bucket.push(value._source);
          });
          response($.map(bucket, function (item) {
            return {
              id: item.id,
              value: item.ru.name + ", " + item.ru.city.name + ", " + item.city.country.code,
              status: item.status
            }
          }
          ))
        }
      });
    },
    minLength: 0,
    select: function(event, ul) {
      $("#hidden-id").val(ul.item.id); //Put Id in a hidden field
      log("id :" + ul.item.id + ", " + ul.item.value + ", status: " + ul.item.status);
    }
  });
});
